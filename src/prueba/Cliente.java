package prueba;

import java.util.List;
import java.util.Map;

import extra.Tupla;
import sistema.SistemaDeTurnos;

public class Cliente {
	
	private static final Fixture F = Fixture.INSTANCE;
	
	public static void main(String[] args) {
		
		SistemaDeTurnos sistema = new SistemaDeTurnos("Sistema");
				
		sistema.registrarPersona(
				F.dniFrodo, 
				"Frodo", 
				23,  
				!F.trabaja,
				!F.tieneEnfPrevia
		);
		sistema.registrarPersona(
				F.dniEowyn,
				"Eowyn",
				25,  
				!F.trabaja,
				F.tieneEnfPrevia
		);
		sistema.registrarPersona(
				F.dniBilbo,
				"Bilbo", 
				65,  
				!F.trabaja,
				F.tieneEnfPrevia
		);
		sistema.registrarPersona(
				F.dniGandalf, 
				"Gandalf", 
				70, 
				F.trabaja,
				!F.tieneEnfPrevia
		);
		sistema.registrarPersona(
				F.dniLegolas, 
				"Legolas", 
				80,
				F.trabaja,
				!F.tieneEnfPrevia
		);
		sistema.registrarPersona(
				F.dniGaladriel, 
				"Galadriel", 
				81, 
				F.trabaja,
				!F.tieneEnfPrevia
		);
		sistema.registrarPersona(
				F.dniArwen, 
				"Arwen", 
				50,  
				F.trabaja,
				!F.tieneEnfPrevia
			);
		
		
		// frodo es el presidente
		// lo registra como votante y le asigna turno
		final Integer numMesaEnfPreexistente = sistema.
				crearMesa(F.enfPreexistente, F.dniFrodo); 
		
		final Integer numMesaMayor65 = sistema.
				crearMesa(F.mayor65, F.dniBilbo);
		
		final Integer numMesaGeneral = sistema.
				crearMesa(F.general, F.dniGaladriel);
		
		final Integer numMesaTrabajador = sistema.
				crearMesa(F.trabajador, F.dniGandalf);
		
		
		System.out.println("Numeros de mesa generados: " + 
				numMesaEnfPreexistente + " " + numMesaMayor65  + " " + numMesaGeneral  + " " + numMesaTrabajador);
	
		// hacer el toString de tupla!
		System.out.println("Turnos generados [Paso 1]: "); 
		System.out.println("\t- " + sistema.consultarTurnoPersona(F.dniFrodo));
		System.out.println("\t- " + sistema.consultarTurnoPersona(F.dniBilbo));
		System.out.println("\t- " + sistema.consultarTurnoPersona(F.dniGaladriel));
		System.out.println("\t- " + sistema.consultarTurnoPersona(F.dniGandalf));

		System.out.println("\n======================================================"); 
		System.out.println("Estado Sistema De Turnos: ");
		System.out.println("------------------------- ");
		System.out.println(sistema.toString());
		System.out.println("======================================================\n"); 
		
		
		sistema.registrarPersona(1, "Nombre1", 30, false, false);
		sistema.registrarPersona(2, "Nombre2", 70, false, false);
		sistema.registrarPersona(3, "Nombre3", 30, false, true);
		sistema.registrarPersona(4, "Nombre4", 30, true, false);

		sistema.asignarTurnoAutomatico();
		// List<Tupla<TipoMesa, Cant Votantes Sin Turno>>
		List<Tupla<String, Integer>> votantesSinTurno = sistema.sinTurnoSegunTipoMesa();
//		sistema.votar(1);

		System.out.println("Cant votantes sin turno :" + votantesSinTurno.size());
	
		Map<Integer,List<Integer>> MesaEnfPreexistente = sistema.asignadosAMesa(numMesaEnfPreexistente);
		Map<Integer,List<Integer>> MesaMayor65 = sistema.asignadosAMesa(numMesaMayor65);
		Map<Integer,List<Integer>> MesaGeneral = sistema.asignadosAMesa(numMesaGeneral);
		Map<Integer,List<Integer>> MesaTrabajador = sistema.asignadosAMesa(numMesaTrabajador);
		
		
		System.out.println("Cant Turnos generados [Paso 2]:"); 
		System.out.println("\t- " + MesaEnfPreexistente.size());
		System.out.println("\t- " + MesaMayor65.size());
		System.out.println("\t- " + MesaGeneral.size());
		System.out.println("\t- " + MesaTrabajador.size());

		//Franja -> List<Dni>
		Map<Integer, List<Integer>> franjaHoraria1 = sistema.asignadosAMesa(numMesaEnfPreexistente);
		Map<Integer, List<Integer>> franjaHoraria2 = sistema.asignadosAMesa(numMesaMayor65);
		Map<Integer, List<Integer>> franjaHoraria3 = sistema.asignadosAMesa(numMesaGeneral);
		Map<Integer, List<Integer>> franjaHoraria4 = sistema.asignadosAMesa(numMesaTrabajador);
		
		System.out.println("Cant Turnos generados [Paso 3]:");
		System.out.println("\t- " + franjaHoraria1.size());
		System.out.println("\t- " + franjaHoraria2.size());
		System.out.println("\t- " + franjaHoraria3.size());
		System.out.println("\t- " + franjaHoraria4.size());

		
		System.out.println("\n======================================================"); 
		System.out.println("Estado Sistema De Turnos: ");
		System.out.println("------------------------- ");
		System.out.println(sistema.toString());
		System.out.println("======================================================\n"); 
		
		
	}

}
