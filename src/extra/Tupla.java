package extra;

public class Tupla<C, S> {

	final C clave;
	final S valor;

	public Tupla(C clave, S valor) {
		this.clave = clave;
		this.valor = valor;
	}

	public C getX() {
		return clave;
	}

	public S getY() {
		return valor;
	}

	@Override
	public String toString() {
		StringBuilder builder = new StringBuilder();
		builder.append("[Numero de mesa: ");
		builder.append(clave);
		builder.append(", franja horaria: ");
		builder.append(valor);
		builder.append("]");
		return builder.toString();
	}
}
