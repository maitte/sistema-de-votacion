package sistema;

import java.util.HashMap;
import java.util.Map;
import java.util.Map.Entry;

import extra.Tupla;

public class MesaEnfermo extends Mesa {
	private Map<Integer, Integer> cupoPorFranjaHoraria;
	private int cantTurnos;
	private int cantCuposTotales;

	public MesaEnfermo(int nroDeMesa, Persona presidente) {
		super(nroDeMesa, presidente);
		this.cupoPorFranjaHoraria = new HashMap<>();
		for (int i = 8; i < 18; i++) {
			this.cupoPorFranjaHoraria.put(i, 20);
		}
		this.cupoPorFranjaHoraria.put(8, cupoPorFranjaHoraria.get(8) - 1);
		this.cantTurnos = 1; // ya se le asigno 1 al presidente, por eso comienza en 1
		this.cantCuposTotales = 199; // 1 menos por el presidente
	}

	@Override
	public Tupla<Integer, Integer> asignarTurno(Persona persona) {
		if (persona.tieneEnfermedad() && !persona.esTrabajador()) {
			for (Entry<Integer, Integer> cupoYFranja : cupoPorFranjaHoraria.entrySet()) {
				if (franjaTieneCupo(cupoYFranja.getValue())) {

					cantCuposTotales--;
					cantTurnos++;
					int franja = cupoYFranja.getKey();
					int cupo = cupoYFranja.getValue() - 1;

					cupoPorFranjaHoraria.put(franja, cupo);

					Tupla<Integer, Integer> turno = new Tupla<Integer, Integer>(this.consultarNroDeMesa(),
							cupoYFranja.getKey());
					return turno;
				}
			}
		}
		return null;
	}

	@Override
	public int consultarCupo() {
		return cantCuposTotales;
	}

	@Override
	public int cantTurnosAsignados() {
		return cantTurnos;
	}

	@Override
	public int cantTurnosAsignados(String tipoMesa) {
		if (tipoMesa.equals("Enf_Preex")) {
			return cantTurnos;
		}
		return 0;
	}

	private boolean franjaTieneCupo(Integer n) {
		return n > 0;
	}

	@Override
	public String toString() {
		StringBuilder builder = new StringBuilder();
		builder.append("Mesa Enfermo");
		builder.append(super.toString());
		builder.append("[Cupo por franja horaria (hora-cupo): ");
		builder.append(cupoPorFranjaHoraria);
		builder.append("]");
		return builder.toString();
	}
}
