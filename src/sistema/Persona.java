package sistema;

public class Persona {
	private int dni;
	private String nombre;
	private int edad;
	private boolean trabajador;
	private boolean enfermedad;

	public Persona(int dni, String nombre, int edad, boolean trabajador, boolean enfermedad) {
		this.dni = dni;
		this.nombre = nombre;
		this.edad = edad;
		this.trabajador = trabajador;
		this.enfermedad = enfermedad;
	}

	public boolean esMayor() {
		return edad >= 65;
	}

	public boolean esTrabajador() {
		return trabajador;
	}

	public boolean tieneEnfermedad() {
		return enfermedad;
	}

	public int getDni() {
		return dni;
	}

	public String toString() {
		StringBuilder datos = new StringBuilder();
		datos.append("Nombre:").append(nombre).append("\n").append("DNI:").append(dni).append("\n").append("Edad:")
				.append(edad).append("\n");
		if (enfermedad) {
			datos.append("Enfermedad:").append("Si\n");
		} else {
			datos.append("Enfermedad:").append("No\n");
		}
		if (trabajador) {
			datos.append("Trabajador:").append("Si\n");
		} else {
			datos.append("Trabajador:").append("No\n");
		}
		return datos.toString();
	}

	public String obtenerNombreYDni() {
		StringBuilder datos = new StringBuilder();
		datos.append(nombre).append(", ").append("DNI:").append(dni);

		return datos.toString();
	}

}
