package sistema;

import java.util.HashMap;
import java.util.LinkedList;
import java.util.List;
import java.util.Map;
import java.util.Map.Entry;

import extra.Tupla;

public class SistemaDeTurnos {
	private Map<Integer, Persona> personasRegistradas; // dni, persona
	private Map<Integer, Tupla<Integer, Integer>> personasConTurno; // Dni, Turno(num mesa, franja)
	private Map<Integer, Tupla<Integer, Integer>> personasQueVotaron;// dni,turno(num mesa, franja)
	private Map<Integer, Mesa> mesas; // nro mesa, mesa
	private String nombreSistema;
	private int cantidadDeMesas;

	public SistemaDeTurnos(String nombreSistema) {
		if (nombreSistema == null) {
			throw new RuntimeException("El nombre del sistema esta vacio");
		}
		this.nombreSistema = nombreSistema;
		this.personasRegistradas = new HashMap<Integer, Persona>();
		this.personasConTurno = new HashMap<Integer, Tupla<Integer, Integer>>();
		this.personasQueVotaron = new HashMap<Integer, Tupla<Integer, Integer>>();
		this.mesas = new HashMap<Integer, Mesa>();
		this.cantidadDeMesas = 0;
	}

	/*
	 * Registrar a los votantes. Antes de asignar un turno el votante debe estar
	 * registrado en el sistema. Si no lo está, se genera una excepción. Si la edad
	 * es menor a 16, se genera una excepción
	 */

	public void registrarPersona(int dni, String nombre, int edad, boolean trabajador, boolean enfermedad) {
		if (edad < 16) {
			throw new RuntimeException("Es menor de 16 años");
		}
		if (estaRegistrado(dni)) {
			throw new RuntimeException("Ya se encuentra registrada la persona");
		}
		Persona p = new Persona(dni, nombre, edad, trabajador, enfermedad);
		personasRegistradas.put(dni, p);
	}

	/*
	 * Agregar una nueva mesa del tipo dado en el parámetro y asignar el presidente
	 * de cada una, el cual deberá estar entre los votantes registrados y sin turno
	 * asignado. - Devuelve el número de mesa creada. si el president es un votante
	 * que no está registrado debe generar una excepción si el tipo de mesa no es
	 * válido debe generar una excepción Los tipos válidos son: “Enf_Preex”,
	 * “Mayor65”, “General” y “Trabajador”
	 */
	public int crearMesa(String tipoMesa, Integer dni) {
		if (!estaRegistrado(dni)) {
			throw new RuntimeException("La persona no está registrada");
		}
		if (tieneTurno(dni)) {
			throw new RuntimeException("La persona tiene turno, no puede ser presidente");
		}
		if (!esMesaValida(tipoMesa)) {
			throw new RuntimeException("Mesa invalida");
		}
		
		int nroMesa = ++cantidadDeMesas;
		Persona presidente = personasRegistradas.get(dni);
		
		if (tipoMesa.equals("Trabajador")) {
			mesas.put(nroMesa, new MesaTrabajador(nroMesa, presidente));
			// Se le asigna turno al presidente y la mesa se inicializa con un cupo menos
			personasConTurno.put(dni, new Tupla<Integer, Integer>(nroMesa, 8));
		}
		if (tipoMesa.equals("Mayor65")) {
			mesas.put(nroMesa, new MesaMayor(nroMesa, presidente));
			personasConTurno.put(dni, new Tupla<Integer, Integer>(nroMesa, 8));
		}
		if (tipoMesa.equals("Enf_Preex")) {
			mesas.put(nroMesa, new MesaEnfermo(nroMesa, presidente));
			personasConTurno.put(dni, new Tupla<Integer, Integer>(nroMesa, 8));
		}
		if (tipoMesa.equals("General")) {
			mesas.put(nroMesa, new MesaGeneral(nroMesa, presidente));
			personasConTurno.put(dni, new Tupla<Integer, Integer>(nroMesa, 8));
		}
		return nroMesa;
	}

	/*
	 * Asigna turnos automáticamente a los votantes sin turno. El sistema busca si
	 * hay alguna mesa y franja horaria factible en la que haya disponibilidad.
	 * Devuelve la cantidad de turnos que pudo asignar.
	 */
	public int asignarTurnoAutomatico() {
		int turnosAsignados = 0;

		for (Entry<Integer, Persona> p : personasRegistradas.entrySet()) {
			int dni = p.getKey();
			if (!tieneTurno(dni)) {
				Tupla<Integer, Integer> turno = asignarTurnoEspecifico(dni);
				if (turno != null) {
					turnosAsignados++;
				}
			}
		}
		return turnosAsignados;
	}

	/*
	 * Asigna un turno a un votante determinado. - Si el DNI no pertenece a un
	 * votante registrado debe generar una excepción. - Si el votante ya tiene turno
	 * asignado se devuelve el turno como: Número de Mesa y Franja Horaria. - Si aún
	 * no tiene turno asignado se busca una franja horaria disponible en una mesa
	 * del tipo correspondiente al votante y se devuelve el turno asignado, como
	 * Número de Mesa y Franja Horaria. - Si no hay mesas con horarios disponibles
	 * no modifica nada y devuelve null. (Se supone que el turno permitirá conocer
	 * la mesa y la franja horaria asignada)
	 */

	public Tupla<Integer, Integer> asignarTurnoEspecifico(Integer dni) {
		if (!estaRegistrado(dni)) {
			throw new RuntimeException("La persona no esta registrada");
		}
		if (tieneTurno(dni)) {
			return personasConTurno.get(dni);
		}

		Persona p = personasRegistradas.get(dni);

		for (Mesa m : mesas.values()) {
			if (m.consultarCupo() > 0) {
				Tupla<Integer, Integer> turno = m.asignarTurno(p);
				if (turno != null) {
					personasConTurno.put(dni, turno);
					return turno;
				}
			}
		}
		return null;
	}

	/*
	 * Consultar la cantidad de votantes sin turno asignados a cada tipo de mesa.
	 * Devuelve una Lista de Tuplas donde se vincula el tipo de mesa con la cantidad
	 * de votantes sin turno que esperan ser asignados a ese tipo de mesa. La lista
	 * no puede tener 2 elementos para el mismo tipo de mesa.
	 */

	public List<Tupla<String, Integer>> sinTurnoSegunTipoMesa() {
		List<Tupla<String, Integer>> personasSinTurnoPorMesa = new LinkedList<>();

		// Tipo de mesa y cant de personas sin turno para esa mesa
		// Al usar un HashMap nos aseguramos que no haya repetidos
		HashMap<String, Integer> mesasYSinTurno = new HashMap<String, Integer>();
		mesasYSinTurno.put("Enf_Preex", 0);
		mesasYSinTurno.put("Mayor65", 0);
		mesasYSinTurno.put("General", 0);
		mesasYSinTurno.put("Trabajador", 0);

		// Obtener personas registradas a asignar en una mesa
		for (Entry<Integer, Persona> p : personasRegistradas.entrySet()) { // DNI, Persona
			Integer dni = p.getKey(); 

			if (!tieneTurno(dni)) {
				Persona perSinTurno = p.getValue();
				if (perSinTurno.esTrabajador()) {
					mesasYSinTurno.put("Trabajador", mesasYSinTurno.get("Trabajador") + 1); // si es trabajador, aumento
																							// los sin turno
				} else if (perSinTurno.esMayor()) {
					mesasYSinTurno.put("Mayor65", mesasYSinTurno.get("Mayor65") + 1);
				} else if (perSinTurno.tieneEnfermedad()) {
					mesasYSinTurno.put("Enf_Preex", mesasYSinTurno.get("Enf_Preex") + 1);
				} else if (!perSinTurno.tieneEnfermedad() && !perSinTurno.esMayor() && !perSinTurno.esTrabajador())
					mesasYSinTurno.put("General", mesasYSinTurno.get("General") + 1);
			}
		}

		// Convertir de map a lista de tuplas
		for (Entry<String, Integer> m : mesasYSinTurno.entrySet()) {
			Integer cantPersonasSinTurno = m.getValue();
			if (cantPersonasSinTurno > 0) {
				String tipoDeMesa = m.getKey();
				Tupla<String, Integer> sinTurnoPorMesa = new Tupla<String, Integer>(tipoDeMesa, cantPersonasSinTurno);
				personasSinTurnoPorMesa.add(sinTurnoPorMesa);
			}
		}
		return personasSinTurnoPorMesa;
	}

	/*
	 * Consulta el turno de un votante dado su DNI. Devuelve Mesa y franja horaria.
	 * - Si el DNI no pertenece a un votante genera una excepción. - Si el votante
	 * no tiene turno devuelve null.
	 */
	public Tupla<Integer, Integer> consultarTurnoPersona(int dni) {
		if (!estaRegistrado(dni)) {
			throw new RuntimeException("La persona no está registrada");
		}
		if (tieneTurno(dni)) {
			Tupla<Integer, Integer> turno = personasConTurno.get(dni);
			return turno;
		}
		return null;
	}

	/*
	 * Hace efectivo el voto del votante determinado por su DNI. Si el DNI no está
	 * registrado entre los votantes debe generar una excepción Si ya había votado
	 * devuelve false. Si no, efectúa el voto y devuelve true.
	 */
	public boolean votar(int dni) {
		if (!estaRegistrado(dni)) {
			throw new RuntimeException("La persona no está registrada");
		}
		if (estaRegistrado(dni) && !voto(dni)) {
			personasQueVotaron.put(dni, personasConTurno.get(dni));
			return true;
		}
		return false;
	}

	/*
	 * Cantidad de votantes con Turno asignados al tipo de mesa que se pide. -
	 * Permite conocer cuántos votantes se asignaron hasta el momento a alguno de
	 * los tipos de mesa que componen el sistema de votación. - Si la clase de mesa
	 * solicitada no es válida debe generar una excepción
	 */

	public int votantesConTurno(String tipoMesa) {
		if (!esMesaValida(tipoMesa)) {
			throw new RuntimeException("Mesa invalida");
		}

		int cantVotantesAsignados = 0;
		for (Mesa m : mesas.values()) {
			cantVotantesAsignados += m.cantTurnosAsignados(tipoMesa);
		}

		return cantVotantesAsignados;
	}

	/*
	 * Dado un número de mesa, devuelve una Map cuya clave es la franja horaria y el
	 * valor es una lista con los DNI de los votantes asignados a esa franja. Sin
	 * importar si se presentaron o no a votar. - Si el número de mesa no es válido
	 * genera una excepción. - Si no hay asignados devuelve null.
	 */
	public Map<Integer, List<Integer>> asignadosAMesa(int numMesa) {
		if (!esNroMesaValido(numMesa)) {
			throw new RuntimeException("Numero de mesa invalido");
		}

		// franja horaria, DNIs de personas en franja
		Map<Integer, List<Integer>> personasEnMesa = new HashMap<Integer, List<Integer>>();

		// Entradas de DNI de personas y su Turno
		List<Entry<Integer, Tupla<Integer, Integer>>> dniYTurno = new LinkedList<Entry<Integer, Tupla<Integer, Integer>>>();

		// Conseguir la gente asignada a la mesa y agregar las franjas horarias con
		// personas en el Map
		for (Entry<Integer, Tupla<Integer, Integer>> p : personasConTurno.entrySet()) { // dni, turno:mesa, franja
			int nroMesaDelTurno = p.getValue().getX();

			if (nroMesaDelTurno == numMesa) {
				dniYTurno.add(p);
				Integer franja = p.getValue().getY();// dni, turno:mesa, franja
				personasEnMesa.put(franja, new LinkedList<Integer>()); // franja horaria y lista de dnis
			}
		}

		// Agregar los DNIs a las franjas horarias
		for (Entry<Integer, Tupla<Integer, Integer>> p : dniYTurno) {// dni, turno:mesa, franja
			Integer franja = p.getValue().getY();
			Integer dni = p.getKey();
			personasEnMesa.get(franja).add(dni);
		}

		boolean hayPersonasEnMesa = personasEnMesa.size() > 0;

		return hayPersonasEnMesa ? personasEnMesa : null;
	}

	private boolean esMesaValida(String tipoMesa) {
		return tipoMesa.equals("Enf_Preex") || tipoMesa.equals("Mayor65") || tipoMesa.equals("General")
				|| tipoMesa.equals("Trabajador");
	}

	private boolean esNroMesaValido(int numMesa) {
		return mesas.containsKey(numMesa);
	}

	private boolean estaRegistrado(int dni) {
		return personasRegistradas.containsKey(dni);
	}

	private boolean tieneTurno(int dni) {
		return personasConTurno.containsKey(dni);
	}

	private boolean voto(int dni) {
		return personasQueVotaron.containsKey(dni);
	}

	@Override
	public String toString() {
		StringBuilder builder = new StringBuilder();
		builder.append("Sistema de Turnos de Votación - UNGS \n");
		builder.append("NOMBRE SISTEMA: ");
		builder.append(nombreSistema);
		builder.append("\nPERSONAS REGISTRADAS: \n");
		builder.append(personasRegistradas.values());
		builder.append("\nPERSONAS CON TURNO: \n");
		builder.append("DNI - Mesa - Hora\n");
		builder.append(personasConTurno);
		builder.append("\nPERSONAS QUE VOTARON: \n");
		builder.append(personasQueVotaron);
		builder.append("\nMESAS: \n");
		builder.append(mesas.values());
		return builder.toString();
	}
}
