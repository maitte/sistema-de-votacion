package sistema;

import extra.Tupla;

public abstract class Mesa {
	private int nroDeMesa;
	private Persona presidente;

	public Mesa(int nroDeMesa, Persona presidente) {
		this.nroDeMesa = nroDeMesa;
		this.presidente = presidente;
	}

	public int consultarNroDeMesa() {
		return this.nroDeMesa;
	}

	public abstract Tupla<Integer, Integer> asignarTurno(Persona persona);

	public abstract int consultarCupo();

	public abstract int cantTurnosAsignados();

	public abstract int cantTurnosAsignados(String tipoMesa);

	@Override
	public String toString() {
		StringBuilder builder = new StringBuilder();
		builder.append(" - Nro. de mesa: ");
		builder.append(nroDeMesa);
		builder.append(", presidente: ");
		builder.append(presidente.obtenerNombreYDni());
		builder.append("]");
		return builder.toString();
	}

}
