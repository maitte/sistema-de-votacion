package sistema;

import extra.Tupla;

public class MesaTrabajador extends Mesa {
	private int cantTurnos;

	public MesaTrabajador(int nroDeMesa, Persona presidente) {
		super(nroDeMesa, presidente);
		this.cantTurnos = 1;
	}

	// va de 8 a 12, asi que a todos se les asigna turno a partir de las 8
	public Tupla<Integer, Integer> asignarTurno(Persona persona) {
		if (persona.esTrabajador()) {
			cantTurnos++;
			return new Tupla<Integer, Integer>(this.consultarNroDeMesa(), 8);
		}
		return null;
	}

	@Override
	public int consultarCupo() { // no tiene limite de cupos
		return 1;
	}

	@Override
	public int cantTurnosAsignados() {
		return cantTurnos;
	}

	@Override
	public int cantTurnosAsignados(String tipoMesa) {
		if (tipoMesa.equals("Trabajador")) {
			return cantTurnos;
		}
		return 0;
	}

	@Override
	public String toString() {
		StringBuilder builder = new StringBuilder();
		builder.append("MesaTrabajador");
		builder.append(super.toString());
		builder.append("[Franja horaria: ");
		builder.append("8 a 12 hs.");
		builder.append("] (sin limite de cupos)");
		return builder.toString();
	}

}
